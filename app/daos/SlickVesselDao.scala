package daos

import models.Vessel
import play.api.db.slick.Config.driver.simple._
import play.api.Play.current
import play.api.db.slick._
import scala.concurrent.Future
import models.SlickVessel
import models.SlickVessel._
import models.SlickVessels._
import models.Location
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class SlickVesselDao extends VesselDao {

  private def findByName(name: String) = DB.withSession {
      implicit session =>
        {
          val ves = for {
            v <- vessels if v.name === name
          } yield v
          ves
        }
    }

  def create(vessel: Vessel): Future[Vessel] = DB.withSession {
    throw new Exception()
  }

  def read(): Future[List[Vessel]] = Future {
    DB.withSession {
      implicit session =>
        {
          val result = vessels.list.map {
            v => Vessel(v.name, v.width, v.length, v.draft, Location("Point", Array(v.longitude, v.latitude)))
          }
          result
        }
    }
  }

  def update(name: String, vessel: Vessel): Future[Vessel] = Future {
    DB.withSession {
      implicit session =>
        {
          val slickVessel = SlickVessel(None, vessel.name, vessel.width, vessel.length, vessel.draft,
              vessel.loc.coordinates(0), vessel.loc.coordinates(1))
          val result: Int = findByName(name).update(slickVessel)
          if(result >= 1) vessel
          else throw new Exception(s"Error updating vessel: ${name}")
        }
    }
  }

  def delete(name: String): Future[String] = {
    throw new Exception()
  }
}