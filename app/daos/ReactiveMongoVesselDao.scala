package daos

import scala.concurrent.Future

import models.Vessel
import models.Vessel.vesselFormat
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.JsObject
import play.api.libs.json.Json
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.Cursor
import reactivemongo.core.commands.LastError

class ReactiveMongoVesselDao extends VesselDao {

  private def collection: JSONCollection = ReactiveMongoPlugin.db.collection[JSONCollection]("vessels")

  private def nameSelector(name: String): JsObject = Json.obj("name" -> name)

  private def processDbOperation[T](futureLastError: Future[LastError], data: T, errMsg: String): Future[T] = {
    futureLastError.map {
      case LastError(true, _, _, _, _, _, _) => data
      case LastError(false, _, Some(errorCode), Some(errorMsg), _, _, _) =>
        throw new Exception(s"[${errorCode}] ${errorMsg}")
      case _ => throw new Exception(errMsg)
    }
  }

  def create(vessel: Vessel): Future[Vessel] = {
    processDbOperation(collection.insert(vessel), vessel, s"Error creating vessel [${vessel.name}]")
  }

  def read(): Future[List[Vessel]] = {
    val cursor: Cursor[Vessel] = collection.find(Json.obj()).sort(Json.obj("name" -> -1)).cursor[Vessel]
    val futureVesselList: Future[List[Vessel]] = cursor.collect[List]()
    futureVesselList
  }

  def update(name: String, vessel: Vessel): Future[Vessel] = {
    processDbOperation(collection.update(nameSelector(name), vessel), vessel, s"Error updating vessel [${name}]")
  }

  def delete(name: String): Future[String] = {
    processDbOperation(collection.remove(nameSelector(name), firstMatchOnly = true), name,
      s"Error creating vessel [${name}]")
  }
}