package daos

import models.Vessel
import scala.concurrent.Future

trait VesselDao {

  def create(vessel: Vessel): Future[Vessel]

  def read(): Future[List[Vessel]]

  def update(name: String, vessel: Vessel): Future[Vessel]

  def delete(name: String): Future[String]
}