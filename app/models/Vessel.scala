package models

import play.api.libs.json.Json

case class Vessel(name: String,
                  width: Double,
                  length: Double,
                  draft: Double,
                  loc: Location)

object Vessel {

  implicit val vesselFormat = Json.format[Vessel]
}
