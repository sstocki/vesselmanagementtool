package models

import play.api.libs.json.Json

case class Location(`type`: String, coordinates: Array[Double]) {

  override def toString():String = {
    s"[${coordinates(0)}, ${coordinates(1)}"
  }
}

object Location {

  implicit val locationFormat = Json.format[Location]
}