package models

import play.api.db.slick.Config.driver.simple._

case class SlickVessel(id: Option[Int],
                       name: String,
                       width: Double,
                       length: Double,
                       draft: Double,
                       longitude: Double,
                       latitude: Double)

class Vessels(tag: Tag) extends Table[SlickVessel](tag, "vessels") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def width = column[Double]("width")
  def length = column[Double]("length")
  def draft = column[Double]("draft")
  def longitude = column[Double]("longitude")
  def latitude = column[Double]("latitude")
  def * = (id.?, name, width, length, draft, longitude, latitude) <> (SlickVessel.tupled, SlickVessel.unapply)
}

object SlickVessels {
  val vessels = TableQuery[Vessels]
}