
class VesselService

    @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    @defaultConfig = { headers: @headers }

    constructor: (@$log, @$http, @$q) ->
        @$log.debug "constructing VesselService"

    listVessels: () ->
        @$log.debug "listVessels()"
        deferred = @$q.defer()

        @$http.get("/vessels")
        .success((data, status, headers) =>
                @$log.info("Successfully listed Vessels - status #{status}")
                deferred.resolve(data)
            )
        .error((data, status, headers) =>
                @$log.error("Failed to list Vessels - status #{status}")
                deferred.reject(data)
            )
        deferred.promise

    createVessel: (vessel) ->
        @$log.debug "createVessel #{angular.toJson(vessel, true)}"
        deferred = @$q.defer()

        @$http.post("/vessel", vessel)
        .success((data, status, headers) =>
                @$log.info("Successfully created Vessel - status #{status}")
                deferred.resolve(data)
            )
        .error((data, status, headers) =>
                @$log.error("Failed to create vessel - status #{status}")
                deferred.reject(data)
            )
        deferred.promise

    updateVessel: (name, vessel) ->
        @$log.debug "updateVessel #{angular.toJson(vessel, true)}"
        deferred = @$q.defer()

        @$http.put("/vessel/#{name}", vessel)
        .success((data, status, headers) =>
              @$log.info("Successfully updated Vessel - status #{status}")
              deferred.resolve(data)
            )
        .error((data, status, header) =>
              @$log.error("Failed to update vessel - status #{status}")
              deferred.reject(data)
            )
        deferred.promise

    deleteVessel: (name) ->
        deferred = @$q.defer()

        @$http.delete("/vessel/#{name}")
        .success((data, status, headers) =>
              @$log.info("Successfully deleted Vessel - status #{status}")
              deferred.resolve(data)
            )
        .error((data, status, header) =>
              @$log.error("Failed to deleted vessel - status #{status}")
              deferred.reject(data)
            )
        deferred.promise

servicesModule.service('VesselService', ['$log', '$http', '$q', VesselService])