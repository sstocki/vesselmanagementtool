class UpdateVesselCtrl

  constructor: (@$log, @$location, @$routeParams, @VesselService) ->
      @$log.debug "constructing UpdateVesselController"
      @vessel = {}
      @location = {}
      @findVessel()

  updateVessel: () ->
      @$log.debug "updateVessel()"
      @vessel.loc = { type: "Point", coordinates: [@location.lon, @location.lat] }
      @VesselService.updateVessel(@$routeParams.name, @vessel)
      .then(
          (data) =>
            @$log.debug "Promise returned #{data} Vessel"
            @vessel = data
            @$location.path("/")
        ,
        (error) =>
            @$log.error "Unable to update Vessel: #{error}"
      )

  findVessel: () ->
      # route params must be same name as provided in routing url in app.coffee
      name = @$routeParams.name
      @$log.debug "findVessel route param: #{name}"

      @VesselService.listVessels()
      .then(
        (data) =>
          @$log.debug "Promise returned #{data.length} Vessels"

          # find a vessel with the given name
          # as filter returns an array, get the first object in it, and return it
          @vessel = (data.filter (vessel) -> vessel.name is name)[0]
          @location = { lon: @vessel.loc.coordinates[0], lat: @vessel.loc.coordinates[1] }
      ,
        (error) =>
          @$log.error "Unable to get Vessels: #{error}"
      )

controllersModule.controller('UpdateVesselCtrl', ['$log', '$location', '$routeParams', 'VesselService', UpdateVesselCtrl])