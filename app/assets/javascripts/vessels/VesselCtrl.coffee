
class VesselCtrl

    constructor: (@$log, @VesselService) ->
        @$log.debug "constructing VesselController"
        @vessels = []
        @getAllVessels()

    getAllVessels: () ->
        @$log.debug "getAllVessels()"

        @VesselService.listVessels()
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Vessels"
                @vessels = data
            ,
            (error) =>
                @$log.error "Unable to get Vessels: #{error}"
            )

controllersModule.controller('VesselCtrl', ['$log', 'VesselService', VesselCtrl])