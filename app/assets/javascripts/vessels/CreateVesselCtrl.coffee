
class CreateVesselCtrl

    constructor: (@$log, @$location,  @VesselService) ->
        @$log.debug "constructing CreateVesselController"
        @vessel = {}
        @location = {}

    createVessel: () ->
        @$log.debug "createVessel()"
        @vessel.loc = { type: "Point", coordinates: [@location.lon, @location.lat] }
        @VesselService.createVessel(@vessel)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} Vessel"
                @vessel = data
                @$location.path("/")
            ,
            (error) =>
                @$log.error "Unable to create Vessel: #{error}"
            )

controllersModule.controller('CreateVesselCtrl', ['$log', '$location', 'VesselService', CreateVesselCtrl])