package controllers

import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import scala.concurrent.Future
import reactivemongo.api.Cursor
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import org.slf4j.{LoggerFactory, Logger}
import javax.inject.Singleton
import play.api.mvc._
import play.api.libs.json._
import reactivemongo.core.commands.Count
import reactivemongo.bson.BSONDocument
import com.google.inject.Inject
import daos.VesselDao
import services.VesselService

@Singleton
class VesselController @Inject()(vesselService: VesselService) extends Controller {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[VesselController])

  import models._

  def createVessel = Action.async(parse.json) {
    request =>
      request.body.validate[Vessel].map {
        vessel => {
          vesselService.add(vessel).map {
            createdVessel =>
              logger.debug(s"Successfully added new vessel: ${createdVessel}")
              Created(s"Vessel Created: ${createdVessel}")
          }
        }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def updateVessel(name: String) = Action.async(parse.json) {
    request =>
      request.body.validate[Vessel].map {
        vessel =>
          vesselService.update(name, vessel).map {
            updatedVessel =>
              logger.debug(s"Successfully updated with LastError: ${updatedVessel}")
              Ok(s"Vessel Updated")
          }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def deleteVessel(name: String) = Action.async {
    vesselService.remove(name).map {
      deletedVesselName => {
        logger.debug(s"Successfully deleted vessel: ${deletedVesselName}")
          Ok(s"Vessel Deleted: ${deletedVesselName}")
      }
    }
  }

  def findVessels = Action.async {
    val futureVesselsList: Future[List[Vessel]] = vesselService.getAll()
    val futureVesselsJsonArray: Future[JsArray] = futureVesselsList.map { vessels =>
      Json.arr(vessels)
    }
    futureVesselsJsonArray.map {
      vessels =>
        Ok(vessels(0))
    }
  }

}
