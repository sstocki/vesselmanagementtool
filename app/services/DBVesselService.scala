package services

import com.google.inject.Inject
import daos.VesselDao
import models.Vessel
import scala.util.Success
import scala.util.Failure
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future

class DBVesselService @Inject() (vesselDao: VesselDao) extends VesselService {

  def getAll(): Future[List[Vessel]] = {
    vesselDao.read()
  }

  def add(vessel: Vessel): Future[Vessel] = {
    vesselDao.create(vessel)
  }

  def remove(name: String): Future[String] = {
    vesselDao.delete(name)
  }
  
  def update(name: String, vessel: Vessel): Future[Vessel] = {
    vesselDao.update(name, vessel)
  }
}