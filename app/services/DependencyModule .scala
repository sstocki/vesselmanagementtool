package services

import com.google.inject.Binder
import com.google.inject.Module

import daos.ReactiveMongoVesselDao
import daos.VesselDao

class DependencyModule extends Module {

  def configure(binder: Binder) = {
    binder.bind(classOf[UUIDGenerator]).to(classOf[SimpleUUIDGenerator])
    binder.bind(classOf[VesselDao]).to(classOf[ReactiveMongoVesselDao])
    binder.bind(classOf[VesselService]).to(classOf[DBVesselService])
  }

}