package services

import models.Vessel
import scala.concurrent.Future

trait VesselService {

  def getAll(): Future[List[Vessel]]

  def add(vessel: Vessel): Future[Vessel]

  def remove(name: String): Future[String]

  def update(name: String, vessel: Vessel): Future[Vessel]
}