package controllers

import scala.concurrent._
import duration._
import org.specs2.mutable._
import play.api.libs.json._
import play.api.test._
import play.api.test.Helpers._
import java.util.concurrent.TimeUnit
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.specs2.mock.Mockito
import play.modules.reactivemongo.json.collection.JSONCollection

@RunWith(classOf[JUnitRunner])
class VesselsIT extends Specification with Mockito {

  val timeout: FiniteDuration = FiniteDuration(5, TimeUnit.SECONDS)

  val url = "/vessel"

  "Vessels" should {

    // width should be double not string
    val invalidVessel = Json.obj(
      "name" -> "vesselTest1",
      "width" -> "35.65",
      "length" -> 77.78,
      "draft" -> 2.65,
      "loc" -> Json.obj(
        "type" -> "Point",
        "coordinates" -> Json.arr(
          37.908698,
          -72.982654)))
    
    "create a vessel from valid json" in {
      running(FakeApplication()) {

        val validVessel = Json.obj(
          "name" -> "vesselTest1",
          "width" -> 34.02,
          "length" -> 77.78,
          "draft" -> 2.65,
          "loc" -> Json.obj(
            "type" -> "Point",
            "coordinates" -> Json.arr(
              37.908698,
              -72.982654)))

        val request = FakeRequest.apply(POST, url).withJsonBody(validVessel)
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status mustEqual CREATED
      }
    }

    "fail to create a vessel from invalid json" in {
      running(FakeApplication()) {

        val request = FakeRequest.apply(POST, url).withJsonBody(invalidVessel)
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        contentAsString(response.get) mustEqual "invalid json"
        result.header.status mustEqual BAD_REQUEST
      }
    }

    "update a vessel from valid json" in {
      running(FakeApplication()) {

        val validVessel = Json.obj(
          "name" -> "vesselTest1",
          "width" -> 55.02,
          "length" -> 12.04,
          "draft" -> 2.65,
          "loc" -> Json.obj(
            "type" -> "Point",
            "coordinates" -> Json.arr(
              37.908698,
              -72.982654)))

        val request = FakeRequest.apply(PUT, url + "/vesselTest1").withJsonBody(validVessel)
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status mustEqual OK
      }
    }

    "fail to update a vessel from invalid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(PUT, "/user/Jack/London").withJsonBody(invalidVessel)
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        contentAsString(response.get) mustEqual "invalid json"
        result.header.status mustEqual BAD_REQUEST
      }
    }

    "delete an existing vessel" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(DELETE, url + "/vesselTest1")
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status mustEqual OK
      }
    }

  }
}