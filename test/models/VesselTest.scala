package models

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

import play.api.libs.json.Json
import play.api.libs.json.Json.toJsFieldJsValueWrapper

@RunWith(classOf[JUnitRunner])
class VesselTest extends Specification {

  "Vessel read" should {

    "read a valid Vessel" in {

      val validVessel = Json.obj(
        "name" -> "vesselTest1",
        "width" -> 20.02,
        "length" -> 56.78,
        "draft" -> 3.66,
        "loc" -> Json.obj(
          "type" -> "Point",
          "coordinates" -> Json.arr(
            34.777698,
            -32.987654)))
      val result = validVessel.validate[Vessel]
      println("valid vessel = " + result)
      result.asOpt must beSome
    }

  }

}